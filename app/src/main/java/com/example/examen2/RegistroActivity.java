package com.example.examen2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import database.Usuario;
import database.UsuariosManejador;

public class RegistroActivity extends AppCompatActivity {

    private EditText edtUsuario;
    private EditText edtClave;
    private EditText edtClaveConf;
    private EditText edtNombre;
    private Button btnRegistrar;
    private Button btnLimpiar;
    private UsuariosManejador db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        edtUsuario = (EditText) findViewById(R.id.txtUsuario);
        edtClave = (EditText) findViewById(R.id.txtClave);
        edtClaveConf = (EditText) findViewById(R.id.txtClaveConf);
        edtNombre = (EditText) findViewById(R.id.txtNombre);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        db = new UsuariosManejador(RegistroActivity.this);

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtUsuario.getText().toString().equals("") || edtClave.getText().toString().equals("") || edtClaveConf.getText().toString().equals("") || edtNombre.getText().toString().equals(""))
                {
                    Toast.makeText(RegistroActivity.this, "Favor de llenar todos los comapos", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if (edtClave.getText().toString().equals(edtClaveConf.getText().toString()))
                    {
                        Usuario usuario = new Usuario();

                        usuario.setUsuario(edtUsuario.getText().toString());
                        usuario.setClave(edtClave.getText().toString());
                        usuario.setNombre(edtNombre.getText().toString());

                        db.openDatabase();
                        long idx = db.insertarUsuario(usuario);
                        Toast.makeText(RegistroActivity.this, "Usuario Registrado Como: " + idx, Toast.LENGTH_SHORT).show();
                        db.cerrar();

                        Intent intent = new Intent(RegistroActivity.this, MainActivity.class);
                        startActivityForResult(intent, 0);
                    }
                    else
                    {
                        Toast.makeText(RegistroActivity.this, "ERROR Contaseñas no coinciden", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtUsuario.setText("");
                edtClave.setText("");
                edtClaveConf.setText("");
                edtNombre.setText("");
            }
        });
    }
}
